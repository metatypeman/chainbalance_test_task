﻿using DataWorker.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataWorker.CommonClasses
{
    public class DbConnectionConfig: IDbConnectionConfig
    {
        #region public constructors
        public DbConnectionConfig(IConfiguration configuration)
        {
            var section = configuration.GetSection("DbConnection");
            ServerName = section["ServerName"];
            DataBaseName = section["DataBaseName"];
            ConnectionString = $"Server={ServerName};Database={DataBaseName};Trusted_Connection=True;";
        }
        #endregion

        #region public properties
        public string ServerName { get; private set; }
        public string DataBaseName { get; private set; }
        public string ConnectionString { get; private set; }
        #endregion
    }
}
