﻿using DataWorker.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataWorker.CreateOrderSection
{
    public class OrderCreationConfig: IOrderCreationConfig
    {
        #region public constructors
        public OrderCreationConfig(IConfiguration configuration)
        {
            var section = configuration.GetSection("OrderCreation");
            OutputPath = section["OutputPath"];
            PopularityRating = float.Parse(section["PopularityRating"]);
        }
        #endregion

        #region public properties
        public string OutputPath { get; private set; }
        public float PopularityRating { get; private set; }
        #endregion
    }
}
