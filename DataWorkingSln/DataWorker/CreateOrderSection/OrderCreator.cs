﻿using DataWorker.DAL;
using DataWorker.Interfaces;
using NLog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace DataWorker.CreateOrderSection
{
    public class OrderCreator: IOrderCreator
    {
        #region public constructors
        public OrderCreator(IOrderCreationConfig orderCreationConfig, ApplicationContext applicationContext)
        {
            mOrderCreationConfig = orderCreationConfig;
            mApplicationContext = applicationContext;
        }
        #endregion

        #region public methods
        public void Run()
        {
            var targetRating = mOrderCreationConfig.PopularityRating;

            var orderName = DateTime.Now.ToString("ddMMyyyy");
            var culture = new CultureInfo("en-GB");

            var booksList = mApplicationContext.Books.Where(p => p.Raiting >= targetRating).OrderBy(p => p.Price).Take(100).ToList();

            var sb = new StringBuilder();
            sb.AppendLine(@"<?xml version=""1.0"" encoding=""utf-8"" standalone=""yes""?>");
            sb.AppendLine("<Orders>");
            sb.AppendLine($"    <OrderNumber>Order_{orderName}</OrderNumber>");
            sb.AppendLine($"    <DocDate>{orderName}</DocDate>");
            sb.AppendLine("     <OrderElements>");

            foreach (var item in booksList)
            {
                sb.AppendLine("        <OrderElement>");
                sb.AppendLine($"            <Book_Name>{item.BookName}</Book_Name>");
                sb.AppendLine($"            <Writer_FisrtName>{item.WriterFirstName}</Writer_FisrtName>");
                sb.AppendLine($"            <Writer_LastName>{item.WriterLastName}</Writer_LastName>");
                sb.AppendLine("            <Book_details>");
                sb.AppendLine($"                <Publishdate>{item.PublishDate.ToString("yyyy-MM-dd")}</Publishdate>");
                sb.AppendLine($"                <PagePrice>{item.Price.ToString(culture)}</PagePrice>");
                sb.AppendLine("            </Book_details> ");
                sb.AppendLine("        </OrderElement>");
            }

            sb.AppendLine("    </OrderElements>");
            sb.AppendLine("</Orders>");

            var fileName = Path.Combine(mOrderCreationConfig.OutputPath, $"Order_{orderName}.xml");

            if(File.Exists(fileName))
            {
                File.Delete(fileName);
            }

            File.AppendAllText(fileName, sb.ToString());
        }
        #endregion

        #region private fields
        private readonly IOrderCreationConfig mOrderCreationConfig;
        private readonly ApplicationContext mApplicationContext;
        #endregion
    }
}
