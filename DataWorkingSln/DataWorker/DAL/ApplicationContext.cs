﻿using DataWorker.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataWorker.DAL
{
    public class ApplicationContext : DbContext
    {
        #region public constructors
        public ApplicationContext(IDbConnectionConfig dbConnectionConfig)
        {
            mDbConnectionConfig = dbConnectionConfig;

            Database.EnsureCreated();
        }
        #endregion

        #region protected members
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {        
            optionsBuilder.UseSqlServer(mDbConnectionConfig.ConnectionString);
        }
        #endregion

        #region public fields
        public DbSet<Book> Books { get; set; }
        public DbSet<LogItem> LogItems { get; set; }
        #endregion

        #region private fields
        private readonly IDbConnectionConfig mDbConnectionConfig;
        #endregion
    }
}
