﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DataWorker.DAL
{
    public class Book
    {
        #region public fields
        public int Id { get; set; }

        [StringLength(200)]
        public string BookName { get; set; }

        [StringLength(50)]
        public string WriterFirstName { get; set; }

        [StringLength(50)]
        public string WriterLastName { get; set; }
        public DateTime PublishDate { get; set; }
        public int PageQuantity { get; set; }
        public float Raiting { get; set; }
        public float Price { get; set; }
        #endregion

        #region public methods
        public override string ToString()
        {
            var fullName = GetType().FullName;
            var sb = new StringBuilder();
            sb.AppendLine($"Begin {fullName}");
            sb.AppendLine($"{nameof(Id)} = {Id}");
            sb.AppendLine($"{nameof(BookName)} = {BookName}");
            sb.AppendLine($"{nameof(WriterFirstName)} = {WriterFirstName}");
            sb.AppendLine($"{nameof(WriterLastName)} = {WriterLastName}");
            sb.AppendLine($"{nameof(PublishDate)} = {PublishDate}");
            sb.AppendLine($"{nameof(PageQuantity)} = {PageQuantity}");
            sb.AppendLine($"{nameof(Raiting)} = {Raiting}");
            sb.AppendLine($"{nameof(Price)} = {Price}");
            sb.AppendLine($"End {fullName}");
            sb.AppendLine();
            return sb.ToString();
        }
        #endregion
    }
}
