﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataWorker.DAL
{
    public class LogItem
    {
        #region public fields
        public int Id { get; set; }
        public DateTime WrittenOn { get; set; }
        public string Message { get; set; }
        #endregion

        #region public methods
        public override string ToString()
        {
            var fullName = GetType().FullName;
            var sb = new StringBuilder();
            sb.AppendLine($"Begin {fullName}");
            sb.AppendLine($"{nameof(Id)} = {Id}");
            sb.AppendLine($"{nameof(WrittenOn)} = {WrittenOn}");
            sb.AppendLine($"{nameof(Message)} = {Message}");
            sb.AppendLine($"End {fullName}");
            sb.AppendLine();
            return sb.ToString();
        }
        #endregion
    }
}
