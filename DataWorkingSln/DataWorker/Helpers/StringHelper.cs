﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataWorker.Helpers
{
    public static class StringHelper
    {
        public static string NormalizeSpaces(this string value)
        {
            if(string.IsNullOrWhiteSpace(value))
            {
                return string.Empty;
            }

            while(value.Contains("  "))
            {
                value = value.Replace("  ", " ");
            }

            return value.Trim();
        }
    }
}
