﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataWorker.Interfaces
{
    public interface IDbConnectionConfig
    {
        string ServerName { get; }
        string DataBaseName { get; }
        string ConnectionString { get; }
    }
}
