﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataWorker.Interfaces
{
    public interface IFileLoader
    {
        void Run();
    }
}
