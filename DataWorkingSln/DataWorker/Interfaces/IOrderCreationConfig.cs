﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataWorker.Interfaces
{
    public interface IOrderCreationConfig
    {
        string OutputPath { get; }
        float PopularityRating { get; }
    }
}
