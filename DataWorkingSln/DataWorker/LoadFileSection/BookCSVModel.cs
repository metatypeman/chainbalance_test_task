﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataWorker.LoadFileSection
{
    public class BookCSVModel
    {
        #region public fields
        public string BookName { get; set; }
        public string WriterFirstName { get; set; }
        public string WriterLastName { get; set; }
        public DateTime PublishDate { get; set; }
        public int PageQuantity { get; set; }
        public float Raiting { get; set; }
        public float Price { get; set; }
        #endregion

        #region public methods
        public override string ToString()
        {
            var fullName = GetType().FullName;
            var sb = new StringBuilder();
            sb.AppendLine($"Begin {fullName}");
            sb.AppendLine($"{nameof(BookName)} = {BookName}");
            sb.AppendLine($"{nameof(WriterFirstName)} = {WriterFirstName}");
            sb.AppendLine($"{nameof(WriterLastName)} = {WriterLastName}");
            sb.AppendLine($"{nameof(PublishDate)} = {PublishDate}");
            sb.AppendLine($"{nameof(PageQuantity)} = {PageQuantity}");
            sb.AppendLine($"{nameof(Raiting)} = {Raiting}");
            sb.AppendLine($"{nameof(Price)} = {Price}");
            sb.AppendLine($"End {fullName}");
            sb.AppendLine();
            return sb.ToString();
        }
        #endregion
    }
}
