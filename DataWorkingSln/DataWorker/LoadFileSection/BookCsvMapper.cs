﻿using TinyCsvParser.Mapping;

namespace DataWorker.LoadFileSection
{
    public class BookCsvMapper: CsvMapping<BookCSVModel>
    {
        #region public constructors
        public BookCsvMapper()
        {
            MapProperty(0, x => x.BookName);
            MapProperty(1, x => x.WriterFirstName);
            MapProperty(2, x => x.WriterLastName);
            MapProperty(3, x => x.PublishDate);
            MapProperty(4, x => x.PageQuantity);
            MapProperty(5, x => x.Raiting);
            MapProperty(6, x => x.Price);
        }
        #endregion
    }
}
