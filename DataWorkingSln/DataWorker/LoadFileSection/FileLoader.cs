﻿using DataWorker.DAL;
using DataWorker.Helpers;
using DataWorker.Interfaces;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TinyCsvParser;

namespace DataWorker.LoadFileSection
{
    public class FileLoader: IFileLoader
    {
        #region public constructors
        public FileLoader(IFileLoadingConfig fileLoadingConfig, IDbConnectionConfig dbConnectionConfig, ApplicationContext applicationContext)
        {
            mFileLoadingConfig = fileLoadingConfig;
            mApplicationContext = applicationContext;
            mDbConnectionConfig = dbConnectionConfig;

            var csvParserOptions = new CsvParserOptions(true, ';');
            var csvMapper = new BookCsvMapper();
            mCsvParser = new CsvParser<BookCSVModel>(csvParserOptions, csvMapper);
        }
        #endregion

        #region public methods
        public void Run()
        {
            WriteLog($"Begin loading from file {mFileLoadingConfig.FilePath}");

            var csvItemsList = mCsvParser.ReadFromFile(mFileLoadingConfig.FilePath, Encoding.UTF8).ToList();

            var sb = new StringBuilder();
            sb.AppendLine("begin tran");
            sb.AppendLine("if exists (");
            sb.AppendLine("select *");
            sb.AppendLine("from dbo.Books with (updlock,serializable)");
            sb.AppendLine("where BookName = @val_BookName");
            sb.AppendLine("and WriterFirstName = @val_WriterFirstName");
            sb.AppendLine("and WriterLastName = @val_WriterLastName");
            sb.AppendLine(")");
            sb.AppendLine("begin");
            sb.AppendLine("update dbo.Books");
            sb.AppendLine("set PublishDate = @val_PublishDate, PageQuantity = @val_PageQuantity, Raiting = @val_Raiting, Price = @val_Price");
            sb.AppendLine("where BookName = @val_BookName");
            sb.AppendLine("and WriterFirstName = @val_WriterFirstName");
            sb.AppendLine("and WriterLastName = @val_WriterLastName;");
            sb.AppendLine("end");
            sb.AppendLine("else");
            sb.AppendLine("begin");
            sb.AppendLine("insert into dbo.Books (BookName, WriterFirstName, WriterLastName, PublishDate, PageQuantity, Raiting, Price)");
            sb.AppendLine("values (@val_BookName, @val_WriterFirstName, @val_WriterLastName, @val_PublishDate, @val_PageQuantity, @val_Raiting, @val_Price);");
            sb.AppendLine("end");
            sb.AppendLine("commit tran");

            var sqlStr = sb.ToString();

            using (var connection = new SqlConnection(mDbConnectionConfig.ConnectionString))
            {
                connection.Open();

                foreach (var csvItem in csvItemsList)
                {
                    var csvItemResult = csvItem.Result;
                    csvItemResult.BookName = csvItemResult.BookName.NormalizeSpaces();
                    csvItemResult.WriterFirstName = csvItemResult.WriterFirstName.NormalizeSpaces();
                    csvItemResult.WriterLastName = csvItemResult.WriterLastName.NormalizeSpaces();

                    var command = new SqlCommand(sqlStr, connection);
                    var param = new SqlParameter("@val_BookName", csvItemResult.BookName);
                    command.Parameters.Add(param);
                    param = new SqlParameter("@val_WriterFirstName", csvItemResult.WriterFirstName);
                    command.Parameters.Add(param);
                    param = new SqlParameter("@val_WriterLastName", csvItemResult.WriterLastName);
                    command.Parameters.Add(param);
                    param = new SqlParameter("@val_PublishDate", csvItemResult.PublishDate);
                    command.Parameters.Add(param);
                    param = new SqlParameter("@val_PageQuantity", csvItemResult.PageQuantity);
                    command.Parameters.Add(param);
                    param = new SqlParameter("@val_Raiting", csvItemResult.Raiting);
                    command.Parameters.Add(param);
                    param = new SqlParameter("@val_Price", csvItemResult.Price);
                    command.Parameters.Add(param);
                    command.ExecuteNonQuery();
                }
            }

            WriteLog($"End loading from file {mFileLoadingConfig.FilePath}");
        }
        #endregion

        #region private fields
        private readonly IFileLoadingConfig mFileLoadingConfig;
        private readonly ApplicationContext mApplicationContext;
        private readonly IDbConnectionConfig mDbConnectionConfig;
        private readonly CsvParser<BookCSVModel> mCsvParser;
        #endregion

        #region private methods
        private void WriteLog(string message)
        {
            var newLogItem = new LogItem()
            {
                WrittenOn = DateTime.Now,
                Message = message
            };

            Console.WriteLine(message);

            mApplicationContext.LogItems.Add(newLogItem);
            mApplicationContext.SaveChanges();
        }
        #endregion
    }
}
