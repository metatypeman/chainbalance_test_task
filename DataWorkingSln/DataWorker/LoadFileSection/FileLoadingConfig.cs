﻿using DataWorker.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataWorker.LoadFileSection
{
    public class FileLoadingConfig: IFileLoadingConfig
    {
        #region public constructors
        public FileLoadingConfig(IConfiguration configuration)
        {
            var section = configuration.GetSection("FileLoading");
            FilePath = section["FilePath"];
        }
        #endregion

        #region public properties
        public string FilePath { get; private set; }
        #endregion
    }
}
