﻿using DataWorker.CommonClasses;
using DataWorker.CreateOrderSection;
using DataWorker.DAL;
using DataWorker.Helpers;
using DataWorker.Interfaces;
using DataWorker.LoadFileSection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NLog;
using System;

namespace DataWorker
{
    sealed class Program
    {
        #region entry point
        static void Main(string[] args)
        {
#if !DEBUG
            try
            {
#endif
                var app = new Program();
                app.Run();
#if !DEBUG
            }
            catch(Exception e)
            {
                LogManager.GetCurrentClassLogger().Info(e);
            }
#endif
        }
        #endregion

        #region public constructors
        public Program()
        {
            var conf = new ConfigurationBuilder()
                .SetBasePath(Environment.CurrentDirectory)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: false)
                .AddEnvironmentVariables()
                .Build();

            var serviceProvider = new ServiceCollection()
                .AddSingleton<IFileLoader, FileLoader>()
                .AddSingleton<IOrderCreator, OrderCreator>()
                .AddSingleton<IDbConnectionConfig, DbConnectionConfig>()
                .AddSingleton<IFileLoadingConfig, FileLoadingConfig>()
                .AddSingleton<IOrderCreationConfig, OrderCreationConfig>()
                .AddSingleton<IConfiguration>(conf)
                .AddDbContext<ApplicationContext>()
                .BuildServiceProvider();

            mServiceProvider = serviceProvider;
        }
        #endregion

        #region public methods
        public void Run()
        {
            var mode = GetModeFromCmdArgs();

            if(mode == KindOfMode.None)
            {
                mode = GetModeFromMenu();
            }

            switch(mode)
            {
                case KindOfMode.LoadFile:
                    {
                        var app = mServiceProvider.GetService<IFileLoader>();
                        app.Run();
                    }
                    break;

                case KindOfMode.CreateOrder:
                    {
                        var app = mServiceProvider.GetService<IOrderCreator>();
                        app.Run();
                    }
                    break;

                default: throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
            }
        }
#endregion

#region private subtypes
        private enum KindOfMode
        {
            None,
            LoadFile,
            CreateOrder
        }
        #endregion

        #region private fields
        private readonly IServiceProvider mServiceProvider;
        #endregion

        #region private methods
        private KindOfMode GetModeFromCmdArgs()
        {
            var cmdArgs = Environment.GetCommandLineArgs();

            if(cmdArgs.Length == 1)
            {
                return KindOfMode.None;
            }

            var arg = cmdArgs[1];

            switch(arg)
            {
                case "C":
                case "c":
                case "1":
                    return KindOfMode.CreateOrder;

                case "L":
                case "l":
                case "2":
                    return KindOfMode.LoadFile;

                default: throw new ArgumentOutOfRangeException(nameof(arg), arg, null);
            }
        }

        private KindOfMode GetModeFromMenu()
        {
            Console.WriteLine("You should choose a mode of this program.");
            Console.WriteLine("1. Load file");
            Console.WriteLine("2. Create order");
            Console.WriteLine("Type number of target item and press Enter.");

            var itemNumStr = Console.ReadLine();

            var itemNum = int.Parse(itemNumStr);

            switch(itemNum)
            {
                case 1:
                    return KindOfMode.LoadFile;

                case 2:
                    return KindOfMode.CreateOrder;

                default: throw new ArgumentOutOfRangeException(nameof(itemNum), itemNum, null);
            }
        }
#endregion
    }
}
